﻿using System.Drawing;

namespace UbubiRevitWebSocket
{
    partial class TokenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TokenForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.advanced = new System.Windows.Forms.Button();
            this.Connect = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Session token :";
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(100, 33);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(71, 20);
            this.txtToken.TabIndex = 1;
            // 
            // advanced
            // 
            this.advanced.Location = new System.Drawing.Point(191, 30);
            this.advanced.Name = "advanced";
            this.advanced.Size = new System.Drawing.Size(109, 23);
            this.advanced.TabIndex = 2;
            this.advanced.Text = "Server settings ...";
            this.advanced.UseVisualStyleBackColor = true;
            this.advanced.Click += new System.EventHandler(this.btnAdvanced_Click);
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(222, 111);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(78, 23);
            this.Connect.TabIndex = 3;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(12, 111);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // TokenForm
            // 
            this.AcceptButton = this.Connect;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this.button3;
            this.ClientSize = new System.Drawing.Size(323, 146);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Connect);
            this.Controls.Add(this.advanced);
            this.Controls.Add(this.txtToken);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TokenForm";
            this.Text = "Connection settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Button advanced;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.Button button3;
    }
}