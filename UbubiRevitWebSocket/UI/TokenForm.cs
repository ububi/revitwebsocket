﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;

namespace UbubiRevitWebSocket
{
    public partial class TokenForm : System.Windows.Forms.Form
    {

        private UBUBIConnexionParameters cnxParam;
           
        public TokenForm()
        {
            InitializeComponent();
        }
        
        public static bool GetConnexionParameters(out UBUBIConnexionParameters cnxParam)
        {
            cnxParam = new UBUBIConnexionParameters();
            TokenForm frm = new TokenForm();
            frm.cnxParam = cnxParam;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                cnxParam = frm.cnxParam;
                return true;
            }
            else
            {
                cnxParam = null;
                return false;
            }
        }


        private void btnAdvanced_Click(object sender, EventArgs e)
        {
            Model.UbubiServerParameter serverParam = this.cnxParam.serverParameter;
            if (AdvancedForm.GetServerParameter(ref serverParam))
                this.cnxParam.serverParameter = serverParam;
        }
        
        private void Connect_Click(object sender, EventArgs e)
        {
            this.cnxParam.token = txtToken.Text;
            
            String errors = cnxParam.validateData();
            if (String.IsNullOrEmpty(errors))
            {
                this.cnxParam.token = this.txtToken.Text;
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show(errors, "Error: Invalid connection configurations", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
    }
}
