﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;

namespace UbubiRevitWebSocket
{
    public partial class AdvancedForm : Form
    {
        private Model.UbubiServerParameter serverParam;

        private AdvancedForm()
        {
            InitializeComponent();
            
        }
        
        public static bool GetServerParameter(ref Model.UbubiServerParameter serverParam)
        {
            Model.UbubiServerParameter newServerParam=null;
            if (serverParam == null)
                newServerParam = new Model.UbubiServerParameter();
            else
                newServerParam = new Model.UbubiServerParameter(serverParam);

            AdvancedForm form = new AdvancedForm();
            form.serverParam = newServerParam;

            if (form.ShowDialog() == DialogResult.OK)
            {
                serverParam = newServerParam;
                return true;
            }
            else
                return false;
        }

        private void AdvancedForm_Load(object sender, EventArgs e)
        {
            txtAddress.Text = this.serverParam.address;
            txtPort.Text = this.serverParam.port.ToString();
            txtTimeout.Text = this.serverParam.timeout.ToString();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int tmpPort = -1;
            int tmpTimout = -1;

            this.serverParam.address = txtAddress.Text;
            this.serverParam.port = Int32.TryParse(txtPort.Text, out tmpPort) ? tmpPort : -1;
            this.serverParam.timeout = Int32.TryParse(txtTimeout.Text, out tmpTimout) ? tmpTimout : -1;

            String errors = this.serverParam.validateData();
            if (String.IsNullOrEmpty(errors))
            {
                this.serverParam.save();
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show(errors, "Error: Invalid server configurations", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
