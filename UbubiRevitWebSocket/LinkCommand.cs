﻿using System;
using System.Collections.Generic;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Autodesk.Revit.Attributes;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net;
using UbubiRevitWebSocket;
using Autodesk.Revit.UI.Events;

[TransactionAttribute(TransactionMode.Manual)]
[RegenerationAttribute(RegenerationOption.Manual)]
public class LinkCommand : IExternalCommand
{
    private Socket clientSocket = null;
    private UIDocument uidoc;
    private UIApplication uiapp;
    private UBUBIConnexionParameters cnxParameters = null;
 
    public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
    {
        uidoc = commandData.Application.ActiveUIDocument;
        uiapp = commandData.Application;
        if (TokenForm.GetConnexionParameters(out cnxParameters) == true)
        {
            openConnection(cnxParameters, uidoc);
            uiapp.Idling += new EventHandler<IdlingEventArgs>(app_Idling);  
        }
        //closeConnection();
        return Result.Succeeded;
    }


    public void openConnection(UBUBIConnexionParameters cnxParam, UIDocument uidocOpened)
    {
        IPAddress ipAddress = Dns.GetHostEntry(cnxParam.serverParameter.address).AddressList[0];
        IPEndPoint serverAddress = new IPEndPoint(ipAddress, cnxParam.serverParameter.port);
        clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        
        // Connection to the server
        clientSocket.Connect(serverAddress);
        
        int toSendLen = System.Text.Encoding.ASCII.GetByteCount(cnxParameters.token);
        byte[] toSendBytes = System.Text.Encoding.ASCII.GetBytes(cnxParameters.token);
        byte[] toSendLenBytes = System.BitConverter.GetBytes(toSendLen);

        // Sending token to the server
        clientSocket.Send(toSendLenBytes);
        clientSocket.Send(toSendBytes);

        // Timeout reception 1 ms
        clientSocket.ReceiveTimeout = 1;
    }

    public void closeConnection()
    {
        clientSocket.Close();
    }

    public void app_Idling(Object sender, IdlingEventArgs arg)
    {

        byte[] byteArray = new byte[5];
        clientSocket.Receive(byteArray);

        int byteLength = System.BitConverter.ToInt32(byteArray, 0);
        byte[] receivedBytes = new byte[byteLength];
        clientSocket.Receive(receivedBytes);
        
        String messageFromUbubi = System.Text.Encoding.ASCII.GetString(receivedBytes);

        Debug.WriteLine("Client received: " + messageFromUbubi);
        int id = Int32.Parse(messageFromUbubi);

        ElementId eid = new ElementId(id);
        List<ElementId> ids = new List<ElementId>();
        ids.Add(eid);
        uidoc.Selection.SetElementIds(ids);
        uidoc.RefreshActiveView();
    }

}