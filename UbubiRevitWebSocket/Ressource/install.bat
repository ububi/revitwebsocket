@echo off
(
@echo ^<?xml version="1.0" encoding="utf-8" standalone="no"?^>
@echo ^<RevitAddIns^>
@echo ^<AddIn Type="Command"^>
@echo ^<Assembly^>%~dp0\..\UbubiRevitWebSocket.dll^</Assembly^>
@echo ^<ClientId^>6F19C1C3-B017-4A02-94C3-E519BE544D1A^</ClientId^>
@echo ^<FullClassName^>LinkCommand^</FullClassName^>
@echo ^<Text^>Link with UBUBI^</Text^>
@echo ^<VendorId^>MathieuDupuis^</VendorId^>
@echo ^<VisibilityMode^>AlwaysVisible^</VisibilityMode^>
@echo ^</AddIn^>
@echo ^</RevitAddIns^>
) > "%appData%\Autodesk\Revit\Addins\2017\UbubiRevitWebSocket.addin"
set /p UserInputPath= Instalation complet, press enter to close this window
@echo on