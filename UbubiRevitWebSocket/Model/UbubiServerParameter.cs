﻿using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;

namespace UbubiRevitWebSocket.Model
{
    [Serializable]
    public class UbubiServerParameter:ICloneable
    {
        private const int DEFAULT_TIMOUT = 400;
        private const int DEFAULT_PORT = 5678;
        private const String DEFAULT_ADDRESS = "myUbubiServer.com";
        private const String CONFIG_FILE = "config.bin";

        public string address { get; set; }
        public int port { get; set; }

        public int timeout { get; set; }

        public UbubiServerParameter(UbubiServerParameter serverParameter)
        {
            this.timeout = serverParameter.timeout;
            this.address = serverParameter.address;
            this.port = serverParameter.port;
        }

        public UbubiServerParameter(string address, int port, int timeout)
        {
            this.timeout = timeout;
            this.address = address;
            this.port = port;
        }

        public UbubiServerParameter(string address, int port) : this(address, port, DEFAULT_TIMOUT){ }

        public UbubiServerParameter(string address) : this(address, DEFAULT_PORT) { }

        public UbubiServerParameter(): this(DEFAULT_ADDRESS) { }

        public void save()
        {
            UbubiServerParameter.saveServerParameter(this);
        }

        public object Clone()
        {
            return new Model.UbubiServerParameter(this);
        }

        public String validateData()
        {
            String errors = "";
            
            if(String.IsNullOrWhiteSpace(this.address))
                errors += "\nServer address is required.";

            if (this.port < 1 || this.port > 65535)
                errors += "\nPlease enter a valid port between 1 and 65535.";

            if (this.timeout < 0 || this.timeout > 360000)
                errors += "\nPlease enter a valide connection timeout between 0 and 360000.";

            if (!String.IsNullOrEmpty(errors))
                errors = errors.Remove(0, 1);

            return errors;
        }

        public static UbubiServerParameter loadServerConf()
        {
            try
            {
                IFormatter formatter = new BinaryFormatter();
                using (Stream stream = new FileStream(CONFIG_FILE, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    formatter.Binder = new AllowAllAssemblyVersionsDeserializationBinder();

                    UbubiServerParameter obj = (UbubiServerParameter)formatter.Deserialize(stream);
                    stream.Close();
                    return obj;
                }
                
            }
            catch (Exception e )
            {
                Console.WriteLine("Load error:"+e.Message);
            }
            
            return null;
            
        }

        public static void saveServerParameter(UbubiServerParameter serverParams)
        {
            if (serverParams == null)
            {
                File.Delete("config.bin");
            }
            else
            {
                IFormatter formatter = new BinaryFormatter();
                using (Stream stream = new FileStream(CONFIG_FILE, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    formatter.Serialize(stream, serverParams);
                    stream.Close();
                }
            }
            
        }
    }

    sealed class AllowAllAssemblyVersionsDeserializationBinder : System.Runtime.Serialization.SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            String currentAssembly = Assembly.GetExecutingAssembly().FullName;

            // In this case we are always using the current assembly
            assemblyName = currentAssembly;

            // Get the type using the typeName and assemblyName
            typeToDeserialize = Type.GetType(String.Format("{0}, {1}",typeName, assemblyName));

            return typeToDeserialize;
        }
    }
}
