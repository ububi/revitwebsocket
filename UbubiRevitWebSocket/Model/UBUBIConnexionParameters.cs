﻿using Autodesk.Revit.UI;
using System;
using System.Windows.Forms;
using System.ComponentModel.DataAnnotations;
using UbubiRevitWebSocket.Model;


public class UBUBIConnexionParameters
{
    public string token { get; set; }

    public UbubiServerParameter serverParameter { get; set; }

    public UBUBIConnexionParameters()
    {
        token = "";
        serverParameter = UbubiServerParameter.loadServerConf();
    }

    public String validateData()
    {
        String errors = "";

        if (String.IsNullOrWhiteSpace(this.token))
            errors += "\nConnection token is required.";
        else if (this.token.Length != 5)
            errors += "\nThe connection token must be 5 caracters long.";

        if (serverParameter==null)
            errors += "\nPlease enter a server address in server settings.";
        else
        {
            String serverParamError = serverParameter.validateData();
            if (!String.IsNullOrEmpty(serverParamError))
                errors += "\n"+serverParamError;
        }

        if (!String.IsNullOrEmpty(errors))
            errors = errors.Remove(0, 1);

        return errors;
    }
}
